class PagesController < ApplicationController

  def about
  end

  def tos
  end

  def faq
  end

  def contact  	
  end

  def privacy  	
  end
end
