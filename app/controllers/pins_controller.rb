class PinsController < ApplicationController

  before_action :logged_in_user, except: [:index, :show]
  before_action :set_pin, except: [:index, :new, :create]
  before_action :correct_pin, only: [:destroy, :edit, :update]

  def index
    current_page = params[:page] || 1
    per_page = params[:per_page] || 16
    @pins = Pin.order('cached_votes_score DESC')

    if params[:search] && !params[:search].blank?
      @pins = Pin.search(params[:search])
      users = User.search(params[:search])
      unless users.blank?
        users.each do |user|
          @pins += user.pins
        end
      end
    end

    length = @pins.length

    @pins = WillPaginate::Collection.create(current_page, per_page, length) do |pager|
      pager.replace @pins[pager.offset, pager.per_page].to_a
    end
  end

  def show
    user = @pin.user.id    
		@pins = Pin.where(user_id: user).shuffle[0..7]
  end

  def new
    @pin = current_user.pins.build
  end

  def edit
  end

  def create
    @pin = current_user.pins.build(pin_params)

    respond_to do |format|
      if @pin.save
        format.html { redirect_to @pin }
        format.json { render action: 'show', status: :created, location: @pin }
      else
        format.html { render action: 'new' }
        format.json { render json: @pin.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    if @pin.update(pin_params)
      flash[:success] = 'Post updated.'
      redirect_to @pin
    else
      render :edit
    end
  end

  def like
    @pin.liked_by current_user
    respond_to do |format|
      format.html {redirect_to :back}
      format.js
    end
  end

  def dislike
    @pin.disliked_by current_user
    respond_to do |format|
      format.html {redirect_to :back}
      format.js
    end
  end

  def destroy
    if @pin.destroy
      flash[:success] = 'Post deleted.'
      redirect_to current_user
    end
  end

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_pin
      @pin = Pin.find(params[:id])
    end
    
    def correct_pin
      @pin = current_user.pins.find_by(id: params[:id])      
      if @pin.nil?
        flash[:danger] = 'Unauthorised action!'
        redirect_to root_url
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def pin_params
      params.require(:pin).permit(:content, :title, :photo, :user_id)
    end
end
