class SessionsController < ApplicationController

  # def create    
  #   user = User.find_by(email: params[:session][:email].downcase)
  #   if user && user.authenticate(params[:session][:password])
  #     log_in user
  #     remember user
  #     redirect_back_or user
  #   else
  #     flash.now[:danger] = 'Invalid email/password combination'
  #     render 'new'
  #   end
  # end
  
  def create
    auth = request.env["omniauth.auth"]
    session[:omniauth] = auth.except('extra')
    user = User.sign_in_from_omniauth(auth)
    log_in user
    if session[:forwarding_url]
      redirect_back_or user
    else
      if user.pins.count < 1
        redirect_to root_url
      else
        redirect_back_or user
      end
    end
  end

  def destroy
    log_out if logged_in?
    redirect_to root_url
  end
end