class Pin < ActiveRecord::Base
  
  self.per_page = 20
  acts_as_votable
  belongs_to :user
  validates :title, presence: true
  validates :content, length: { maximum: 1500 }
  has_attached_file :photo, styles: { large: "500x500>", thumb: "220x220>" }
  validates_attachment_content_type :photo, content_type: /\Aimage\/.*\Z/
  # validate :is_picture

  def self.search(search)
    where("title LIKE ? OR content LIKE ?", "%#{search}%", "%#{search}%") 
  end

  # Prettifies the URLs
  def to_param
    "#{id} #{title[0,30]}".parameterize
  end

  private

    # Validates the size of an uploaded picture.
    # def picture_size
    #   if picture.size > 5.megabytes
    #     errors.add(:picture, "should be less than 5MB")
    #   end
    # end

    # def is_picture
    #   errors.add(:picture, "format is invalid.") unless %w(image/jpeg image/png image/jpg image/gif image/tiff image/bmp).include? picture.sanitized_file.content_type
    # end

    # def is_picture
    #   unless picture.blank?
    #     unless %w(image/jpeg image/png image/jpg image/gif image/tiff image/bmp).include? picture.sanitized_file.content_type
    #       errors.add(:picture, "format is invalid.")
    #     end
    #   end
    # end

end