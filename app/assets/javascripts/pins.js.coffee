# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$ ->
  $('#pins').imagesLoaded ->
    $('#pins').masonry
      itemSelector: '.box, .box-sm'
      isFitWidth: true

document.getElementById('img_button').onclick = ->
  document.getElementById('img_file').click()
  return

textarea = document.getElementById('pin-content')
count = document.getElementById('char-count')
button = document.getElementById('trenda-btn')

countChars = (e) ->
	len = textarea.value.length
	count.innerHTML = len
	if len > 1400
		count.className = "limit"
		button.setAttribute "disabled", "disabled"
	else
		count.className = "badge"
		button.removeAttribute "disabled"

textarea.addEventListener "keyup", countChars, false
textarea.addEventListener "keydown", countChars, false