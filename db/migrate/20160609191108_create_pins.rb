class CreatePins < ActiveRecord::Migration

  def self.up
    create_table :pins do |t|
      t.belongs_to :user, index: true

      t.string :title
      t.text :content
      t.string :picture

      t.timestamps
    end
  end

  def self.down
    drop_table :pins
  end
end