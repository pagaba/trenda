class CreateUsers < ActiveRecord::Migration

  def self.up
    create_table :users do |t|
      t.string :provider
      t.string :uid
      t.string :name
      t.string :avatar

      t.timestamps null: false

      t.index :name
    end
  end

  def self.down
  	drop_table :users
  end
end







# class CreateUsers < ActiveRecord::Migration

#   def self.up
#     create_table :users do |t|
#       t.string :provider
#       t.string :uid
#       t.string :name
#       t.string :avatar

#       t.timestamps
#     end
#     add_index :users, :name
#   end

#   def self.down
#     drop_table :users
#   end
# end