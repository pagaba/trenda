# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Trenda::Application.config.secret_key_base = 'f831e2c387f140c343cbc37857940c0eb62c19b9d1eb0de40d5676cf93047b3824282870ab7da6cebe3fde75053fa28bc7d257b135b047db2c29e3a73a4d56aa'
